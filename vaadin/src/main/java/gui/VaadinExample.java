package gui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

public class VaadinExample extends UI {
    private static final long serialVersionUID = 1L;

    private TextField input = new TextField();
    private Button button = new Button("x^2");

    @Override
    protected void init(VaadinRequest request) {
        setContent(new VerticalLayout(input, button));

        button.addClickListener(event -> {
            Integer value = Integer.valueOf(input.getValue());
            input.setValue(String.valueOf(value * value));
        });
    }
}
