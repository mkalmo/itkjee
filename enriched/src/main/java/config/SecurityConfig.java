package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/loginfailed/**").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/**").hasAnyRole("ADMIN", "USER");

        http.formLogin()
            .loginPage("/login")
            .failureUrl("/loginfailed")
            .permitAll();

        http.logout().logoutSuccessUrl("/login");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder registry) throws Exception {

        // https://www.dailycred.com/blog/12/bcrypt-calculator

        registry.inMemoryAuthentication()
                .passwordEncoder(new BCryptPasswordEncoder(10))
                .withUser("user")
                .password("$2a$10$ZagDA/8KjhbcC7EGJgqrjOV/uKul5MhJjuTIisxKCZamRUq0gbcym")
                .roles("USER")
                .and()
                .withUser("admin")
                .password("$2a$10$WID7JrpmmWCGzQODSHjNoOp6/RCn8tgPJWvrjmveeI9m7FwGn5lEu")
                .roles("ADMIN", "USER");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.debug(false);
    }
}