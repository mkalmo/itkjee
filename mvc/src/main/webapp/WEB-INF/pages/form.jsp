<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="styles.jsp"%>
</head>
<body>

  <%@ include file="menu.jsp"%>

  <c:if test="${not empty errors}">
    <div style="color:red">
      <c:forEach var="error" items="${errors}">
        <c:out value="${error}"></c:out><br />
      </c:forEach>
    </div>
    <br/><br/>
  </c:if>

  <c:if test="${empty url}">
      <c:set var="url" value="save"></c:set>
  </c:if>

  <c:url value="/${url}" var="theAction" />
  <form:form method="post" action="${theAction}" modelAttribute="personForm">

    <c:if test="${not empty personForm.message}">
        <div id="messageBlock">${personForm.message}</div>
    </c:if>

    <form:errors path="*" id="messageBlock" cssClass="errorBlock" element="div" />

    <form:input type="hidden" path="person.id" />

    Eesnimi: <form:input id="firstName" path="person.firstName" /><br/>
    Perekonnanimi: <form:input id="name" path="person.name" /><br/>
    Vanusegrupp: <form:select id="ageGroupId"
                              path="person.ageGroupId"
                              items="${personForm.ageGroups}" />

    <br/><br/>

    <form:input type="submit" path="validateButton" value="Valideeri"/>
    <form:input type="submit" path="saveButton" value="Salvesta"/>

  </form:form>
</body>
</html>