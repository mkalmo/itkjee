package mvc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import util.FileUtil;
import util.PropertyLoader;

public class SetupDao {

    static {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void createSchema() {
        String contents = FileUtil.readFileFromClasspath("schema.sql");

        for (String statement : contents.split(";")) {
            if (statement.matches("\\s*")) {
                continue;
            }

            executeUpdate(statement);
        }
    }

    private void executeUpdate(String sql) {
        String url = new PropertyLoader().getProperty("db.url");
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {

            System.out.println("executing: " + sql);

            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
