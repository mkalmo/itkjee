package perfomance;

import util.DbUtil;
import util.FileUtil;
import util.Timer;

import java.sql.*;
import java.text.MessageFormat;

public class Main {

    public static final String URL = "jdbc:mysql://localhost/test"
            + "?useUnicode=true&useJDBCCompliantTimezoneShift=true"
            + "&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public static void main(String[] args) throws Exception {
        new Main().start();
    }

    private void start() throws SQLException {
        createSchema();

        Timer timer = new Timer().start();

        withoutPreparedStatement();

        System.out.println(timer.getElapsedTime());

        createSchema();

        timer = new Timer().start();

        batchMode();

        System.out.println(timer.getElapsedTime());
    }

    private void withoutPreparedStatement() throws SQLException {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {

            for (int i = 1; i <= 1000; i++) {
                String query =
                        MessageFormat.format(
                                "insert into person values ({0}, \"{1}\", {0})",
                                String.valueOf(i), "John");

                stmt.executeUpdate(query);
            }
        }
    }

    private void batchMode() {
        try (Connection conn = getConnection()) {

            String sqlQuery = "insert into person values (?, ?, ?)";
            try (PreparedStatement ps = conn.prepareStatement(sqlQuery)) {
                conn.setAutoCommit(false);
                for (int i = 1; i <= 1000; i++) {
                    ps.setLong(1, i);
                    ps.setString(2, "John");
                    ps.setInt(3, i);
                    ps.addBatch();
                }

                ps.executeBatch();
                conn.commit();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void createSchema() throws SQLException {
        try (Connection conn = getConnection()) {
            DbUtil.insertFromFile(conn, FileUtil.readFileFromClasspath("schema-mysql.sql"));
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, "root", "1");
    }
}
