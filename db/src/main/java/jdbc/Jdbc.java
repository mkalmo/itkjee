package jdbc;

import util.DataSourceProvider;

import javax.sql.DataSource;
import java.sql.*;

public class Jdbc {

    static {
        DataSourceProvider.setDbUrl("jdbc:hsqldb:mem:db1");
    }

    public static void main(String[] args) {
        Jdbc d = new Jdbc();

        d.createTable();
        d.insertData();
        d.printPersons();
        d.printCertainPerson();
        d.printCertainPersonLike();
        d.updatePerson();
    }

    private void printCertainPersonLike() {
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(
                     "SELECT id, name FROM person WHERE upper(name) LIKE upper(?)")) {
            ps.setString(1, "%OH%");

            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    System.out.println(rset.getInt(1) + ", " + rset.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Connection getConnection() throws SQLException {
        return DataSourceProvider.getDataSource().getConnection();
    }

    private void updatePerson() {
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(
                        "UPDATE person SET name = ? WHERE id = ?")) {

            ps.setString(1, "Mary");
            ps.setLong(2, 3L);

            int rowCount = ps.executeUpdate();
            System.out.println(rowCount + " rows updated!");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void printCertainPerson() {
        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(
                     "SELECT id, name FROM person WHERE id = ?")) {
            ps.setLong(1, 3L);

            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    System.out.println(rset.getInt(1) + ", " + rset.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void printPersons() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {

            try (ResultSet rset = stmt.executeQuery("SELECT id, name FROM person")) {
                while (rset.next()) {
                    System.out.println(rset.getLong(1) + ", " + rset.getString(2));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void createTable() {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate("CREATE TABLE person (id INT, name VARCHAR(100))");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void insertData() {
        executeQuery("INSERT INTO person VALUES (1, 'John')");
        executeQuery("INSERT INTO person VALUES (2, 'Jack')");
        executeQuery("INSERT INTO person VALUES (3, 'Jill')");
    }

    private void executeQuery(String queryString) {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(queryString);
         } catch (Exception e) {
             throw new RuntimeException(e);
         }
    }
}

