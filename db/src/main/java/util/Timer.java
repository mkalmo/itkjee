package util;

import java.math.BigDecimal;
import java.math.MathContext;

public class Timer {

    private long startTime;

    public String getElapsedTime() {
        BigDecimal elapsed = new BigDecimal(System.currentTimeMillis() - startTime);
        BigDecimal seconds = elapsed.divide(new BigDecimal(1000), MathContext.DECIMAL32);
        seconds = seconds.setScale(3, BigDecimal.ROUND_HALF_UP);

        return seconds.toPlainString();
    }

    public Timer start() {
        startTime = System.currentTimeMillis();
        return this;
    }
}
