package querydsl;

import com.querydsl.sql.*;
import generated.QComment;
import generated.QPost;
import util.DataSourceProvider;

public class Sample {

    public static void main(String[] args) {
        SQLTemplates templates = new HSQLDBTemplates();
        Configuration configuration = new Configuration(templates);

        SQLQueryFactory queryFactory = new SQLQueryFactory(configuration,
                DataSourceProvider.getDataSource());

        QPost qPost = new QPost("p");
        QComment qComment = new QComment("c");

        SQLQuery<String> query = queryFactory
                .select(qPost.title)
                .from(qPost)
                .where(qPost.title.eq("Post 1"));

        String commentCriteria = "%comment 1";

        if (commentCriteria != null) {
            query.join(qComment).on(qComment.postId.eq(qPost.id))
                    .where(qComment.text.like(commentCriteria));
        }

        System.out.println("sql: " + query.getSQL().getSQL());
        System.out.println("bindings: " + query.getSQL().getBindings());
        System.out.println("query result: " + query.fetch());
    }
}

