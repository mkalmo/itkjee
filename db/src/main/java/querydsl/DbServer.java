package querydsl;

import org.hsqldb.Server;
import util.DbUtil;
import util.FileUtil;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbServer {

    public static final String DB_NAME = "dsl";

    public static void main(String[] args) throws Exception {

        Server server = new Server();

        new Thread(() -> {
            server.setDatabasePath(0, "mem:db1");
            server.setDatabaseName(0, DB_NAME);
            server.start();
        }).start();

        while ("ONLINE".equals(server.getStateDescriptor())) {
            Thread.sleep(500);
        }

        String url = "jdbc:hsqldb:hsql://localhost:9001/" + DB_NAME;
        try (Connection connection = DriverManager.getConnection(url)) {
            DbUtil.insertFromFile(connection, FileUtil.readFileFromClasspath("schema.sql"));
        }
    }

}
