package generated;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QPostTag is a Querydsl query type for QPostTag
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QPostTag extends com.querydsl.sql.RelationalPathBase<QPostTag> {

    private static final long serialVersionUID = -1271275832;

    public static final QPostTag postTag = new QPostTag("POST_TAG");

    public final NumberPath<Long> postId = createNumber("postId", Long.class);

    public final NumberPath<Long> tagId = createNumber("tagId", Long.class);

    public final com.querydsl.sql.PrimaryKey<QPostTag> sysPk10117 = createPrimaryKey(postId, tagId);

    public QPostTag(String variable) {
        super(QPostTag.class, forVariable(variable), "PUBLIC", "POST_TAG");
        addMetadata();
    }

    public QPostTag(String variable, String schema, String table) {
        super(QPostTag.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QPostTag(Path<? extends QPostTag> path) {
        super(path.getType(), path.getMetadata(), "PUBLIC", "POST_TAG");
        addMetadata();
    }

    public QPostTag(PathMetadata metadata) {
        super(QPostTag.class, metadata, "PUBLIC", "POST_TAG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(postId, ColumnMetadata.named("POST_ID").withIndex(1).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(tagId, ColumnMetadata.named("TAG_ID").withIndex(2).ofType(Types.BIGINT).withSize(64).notNull());
    }

}

