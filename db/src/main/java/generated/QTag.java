package generated;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTag is a Querydsl query type for QTag
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTag extends com.querydsl.sql.RelationalPathBase<QTag> {

    private static final long serialVersionUID = -1689232760;

    public static final QTag tag = new QTag("TAG");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final com.querydsl.sql.PrimaryKey<QTag> sysPk10110 = createPrimaryKey(id);

    public QTag(String variable) {
        super(QTag.class, forVariable(variable), "PUBLIC", "TAG");
        addMetadata();
    }

    public QTag(String variable, String schema, String table) {
        super(QTag.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTag(Path<? extends QTag> path) {
        super(path.getType(), path.getMetadata(), "PUBLIC", "TAG");
        addMetadata();
    }

    public QTag(PathMetadata metadata) {
        super(QTag.class, metadata, "PUBLIC", "TAG");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(name, ColumnMetadata.named("NAME").withIndex(2).ofType(Types.VARCHAR).withSize(255).notNull());
    }

}

