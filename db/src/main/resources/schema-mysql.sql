DROP TABLE IF EXISTS person;

CREATE TABLE person (
    id int NOT NULL,
    name varchar(255) NOT NULL,
    age int,
    PRIMARY KEY (ID)
);
