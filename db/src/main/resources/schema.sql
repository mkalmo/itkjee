DROP SCHEMA public CASCADE;

CREATE TABLE POST (
  id BIGINT NOT NULL PRIMARY KEY,
  title VARCHAR(255) NOT NULL
);

CREATE TABLE COMMENT (
  id BIGINT NOT NULL PRIMARY KEY,
  post_id BIGINT NOT NULL,
  text VARCHAR(255) NOT NULL,
  FOREIGN KEY (post_id) REFERENCES POST ON DELETE CASCADE
);

CREATE TABLE TAG (
  id BIGINT NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE POST_TAG (
  post_id BIGINT NOT NULL,
  tag_id BIGINT NOT NULL,
  PRIMARY KEY (post_id, tag_id)
);

INSERT INTO POST (id, title) VALUES (1, 'Post 1');
INSERT INTO POST (id, title) VALUES (2, 'Post 2');

INSERT INTO COMMENT (id, post_id, text) VALUES (10, 1, 'Post 1 comment 1');
INSERT INTO COMMENT (id, post_id, text) VALUES (11, 1, 'Post 1 comment 2');
INSERT INTO COMMENT (id, post_id, text) VALUES (12, 2, 'Post 2 comment 1');
INSERT INTO COMMENT (id, post_id, text) VALUES (13, 2, 'Post 2 comment 2');

INSERT INTO TAG (id, name) VALUES (21, 'Tag 1');
INSERT INTO TAG (id, name) VALUES (22, 'Tag 2');

INSERT INTO POST_TAG (post_id, tag_id) VALUES (1, 21);
INSERT INTO POST_TAG (post_id, tag_id) VALUES (1, 22);
